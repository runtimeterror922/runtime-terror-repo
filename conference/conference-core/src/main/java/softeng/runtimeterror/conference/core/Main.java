package softeng.runtimeterror.conference.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import softeng.runtimeterror.conference.core.model.Author;

import java.util.List;

public class Main {
    private static final Logger LOG = LoggerFactory.getLogger(Main.class);

//    public static void main(String[] args)
//    {
//        try {
//            Author a = new Author("Miguel", "Cervantes", "miguel@gmail.com", "miguel1616rip", "donquijote");
//            Author b = new Author(null, "Goethe", "goethe@yahoo.com", "goethe1832rip", "faust");
//            Author c = new Author("Edgar Allan", "Poe", "poe@gmail.com", "poe1849rip", "blackcat");
//
//            EntityDAO<Author> ad = DAOFactory.getAuthorDAO();
//
//            // test insert
//            System.out.println("\ninserting entities");
//            ad.insert(a);
//            ad.insert(b);
//            ad.insert(c);
//
//            // test update
//            System.out.println("\nupdate test");
//            b.setFirstName("NEW");
//            b.setLastName("NEW");
//            ad.update(b);
//
//            // test delete
//            System.out.println("\ndelete test");
//            ad.delete(a);
//
//            // test deleteById
//            System.out.println("\ndeleteById test");
//            //Long id = resultlist.get(0).getId();
//            ad.deleteById(3L);
//
//            // test findAll
//            System.out.println("\nfindAll test");
//            List<Author> resultlist2 = ad.findAll();
//            if (resultlist2.size() != 0)
//                resultlist2.forEach(System.out::println);
//
//            // TODO some instructions:
//            //  database is in drop-n-create mode, so its not yet persistent, going to be recreated at every launch
//            //  you need to run the /diagrams_n_stuff/create_database.sql script ONCE in mysqlwokrbench to create the database
//            //  .
//            //  SELECT queries dont need transaction wrappers
//            //  "SELECT *" can be omitted from queries
//            //  .
//            //  create a logger in every service/repository class
//            //  LOG.trace, LOG.debug - written only to file
//            //  LOG.info, LOG.warn, LOG.error - both console and file
//            //  all executed sql statements are logged into console - useful for debugging
//        }
//        catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
}
