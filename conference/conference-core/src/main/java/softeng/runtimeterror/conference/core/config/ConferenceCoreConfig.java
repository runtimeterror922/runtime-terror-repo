package softeng.runtimeterror.conference.core.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({"softeng.runtimeterror.conference.core.repository",
                "softeng.runtimeterror.conference.core.service"})
public class ConferenceCoreConfig {
}
