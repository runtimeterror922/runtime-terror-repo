package softeng.runtimeterror.conference.core.model;

import lombok.Data;


import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;

@Data
public class AbstractModel<ID> implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private ID id;

    public AbstractModel(ID id) {
        this.id = id;
    }

    public AbstractModel() {
        this.id = null;
    }
}