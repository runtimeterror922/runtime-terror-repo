package softeng.runtimeterror.conference.core.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Author extends AbstractModel<Long> {

    private String firstName;
    private String lastName;
    private String email;
    private String username;
    private String password;
    private Boolean validated;

    public Author(Long aLong, String firstName, String lastName, String email, String username, String password, Boolean validated) {
        super(aLong);
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.username = username;
        this.password = password;
        this.validated = validated;
    }

    @Override
    public String toString() {
        return "Author{" +
                "ID='" + super.getId() + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", username='" + username + '\'' +
                ", validated='" + validated + '\'' +
                '}';
    }
}
