package softeng.runtimeterror.conference.core.model;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Conference extends AbstractModel<Long> {

    private String name;
    private String callForPapers;
    private Date startingTime;
    private Date endingTime;
    private Date abstractDeadline;
    private Date fullpaperDeadline;
    private Date biddingDeadline;

    @OneToMany
    private Set<Reviewer> reviewers;
    @OneToMany
    private Set<Section> sections;
    @OneToMany
    private Set<Author> authors;
    private Set<Long> submittedPapersIds;
    private Set<Long> acceptedPapersIds;

    @Override
    public String toString() {
        return "Conference{" +
                "name='" + name + '\'' +
                ", callForPapers='" + callForPapers + '\'' +
                ", startingTime=" + startingTime +
                ", endingTime=" + endingTime +
                ", abstractDeadline=" + abstractDeadline +
                ", fullpaperDeadline=" + fullpaperDeadline +
                ", biddingDeadline=" + biddingDeadline +
                ", reviewers=" + reviewers +
                ", sections=" + sections +
                ", authors=" + authors +
                ", submittedPapersIds=" + submittedPapersIds +
                ", acceptedPapersIds=" + acceptedPapersIds +
                '}';
    }
}
