package softeng.runtimeterror.conference.core.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Evaluation extends AbstractModel<Long> {

    @Enumerated(EnumType.STRING)
    private Mark mark;
    private String suggestion;
    private Boolean accepted;

    public enum Mark {
        STRONGACCEPT,
        ACCEPT,
        WEAKACCEPT,
        BORDERLINE,
        WEAKREJECT,
        REJECT,
        STRONGREJECT
    }

    @Override
    public String toString() {
        return "Evaluation{" +
                "ID='" + super.getId() + '\'' +
                ", evaluation='" + mark + '\'' +
                ", suggestion='" + suggestion + '\'' +
                ", accepted='" + accepted + '\'' +
                '}';
    }
}
