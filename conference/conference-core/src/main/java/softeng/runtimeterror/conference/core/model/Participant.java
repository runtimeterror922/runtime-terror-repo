package softeng.runtimeterror.conference.core.model;

import lombok.*;

import javax.persistence.Entity;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Participant extends AbstractModel<Long> {

    private String firstName;
    private String lastName;
    private Boolean payed;
    private Boolean steeringCommitteeMember;

    @Override
    public String toString() {
        return "Participant{" +
                "ID='" + super.getId() + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", payed=" + payed + '\'' +
                ", steeringCommitteeMember=" + steeringCommitteeMember.toString() + '\'' +
                '}';
    }
}
