package softeng.runtimeterror.conference.core.model;

import lombok.*;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Proposal extends AbstractModel<Long> {

    private String proposalTitle;
    private String keywords;
    private String topics;
    private Set<Long> authorIds;
    private Long presentedByAuthorId;
    private String abstractPaper; // TODO later this needs to be a file somehow
    private String fullPaper; // TODO later this needs to be a file somehow
    @OneToMany
    private List<Evaluation> evaluations;

    @Override
    public String toString() {
        return "Proposal{" +
                "ID='" + super.getId() + '\'' +
                ", proposalTitle='" + proposalTitle + '\'' +
                ", keywords='" + keywords + '\'' +
                ", topics='" + topics + '\'' +
                '}';
    }
}
