package softeng.runtimeterror.conference.core.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Reviewer extends AbstractModel<Long> {

    private String firstName;
    private String lastName;
    private Type type;
    private Boolean isDeadlineResponsible;
    private String affiliation;
    private String email;
    private String username;
    private String password;
    private String website;
    private Set<Long> wouldEvaluateProposals;
    private Set<Long> refuseEvaluateProposals;

    public enum Type {
        PCMEMBER,
        CHAIR,
        COCHAIR
    }

    @Override
    public String toString() {
        return "Reviewer{" +
                "ID='" + super.getId() + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", type='" + type + '\'' +
                ", isDeadlineResponsible='" + isDeadlineResponsible + '\'' +
                ", affiliation='" + affiliation + '\'' +
                ", email='" + email + '\'' +
                ", username='" + username + '\'' +
                ", website='" + website + '\'' +
                '}';
    }
}
