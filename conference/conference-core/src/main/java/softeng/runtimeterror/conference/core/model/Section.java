package softeng.runtimeterror.conference.core.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Section extends AbstractModel<Long> {

    private String room;
    private Long sectionChairId;
    @OneToMany
    private Set<Participant> participants;
    private Set<Long> presentedPapersIds;

}
