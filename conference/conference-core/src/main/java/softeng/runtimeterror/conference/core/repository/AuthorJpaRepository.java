package softeng.runtimeterror.conference.core.repository;

import org.springframework.stereotype.Repository;
import softeng.runtimeterror.conference.core.model.Author;

public interface AuthorJpaRepository extends MyJpaRepository<Author, Long> {
}
