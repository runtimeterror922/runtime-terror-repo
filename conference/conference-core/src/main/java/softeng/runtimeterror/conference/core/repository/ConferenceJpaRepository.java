package softeng.runtimeterror.conference.core.repository;

import org.springframework.stereotype.Repository;
import softeng.runtimeterror.conference.core.model.Conference;

public interface ConferenceJpaRepository extends MyJpaRepository<Conference, Long> {
}
