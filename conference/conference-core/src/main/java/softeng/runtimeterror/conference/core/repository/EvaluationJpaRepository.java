package softeng.runtimeterror.conference.core.repository;

import org.springframework.stereotype.Repository;
import softeng.runtimeterror.conference.core.model.Evaluation;

public interface EvaluationJpaRepository extends MyJpaRepository<Evaluation, Long> {
}
