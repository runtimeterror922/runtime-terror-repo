package softeng.runtimeterror.conference.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import softeng.runtimeterror.conference.core.model.AbstractModel;

@NoRepositoryBean
public interface MyJpaRepository<T extends AbstractModel<ID>, ID> extends JpaRepository<T,ID> {
}
