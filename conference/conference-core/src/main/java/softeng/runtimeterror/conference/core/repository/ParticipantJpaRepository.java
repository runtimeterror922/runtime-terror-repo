package softeng.runtimeterror.conference.core.repository;

import org.springframework.stereotype.Repository;
import softeng.runtimeterror.conference.core.model.Participant;

public interface ParticipantJpaRepository extends MyJpaRepository<Participant, Long> {
}
