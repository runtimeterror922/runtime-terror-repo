package softeng.runtimeterror.conference.core.repository;

import org.springframework.stereotype.Repository;
import softeng.runtimeterror.conference.core.model.Proposal;

public interface ProposalJpaRepository extends MyJpaRepository<Proposal, Long> {
}
