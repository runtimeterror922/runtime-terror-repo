package softeng.runtimeterror.conference.core.repository;

import org.springframework.stereotype.Repository;
import softeng.runtimeterror.conference.core.model.Reviewer;

public interface ReviewerJpaRepository extends MyJpaRepository<Reviewer, Long> {
}
