package softeng.runtimeterror.conference.core.repository;

import org.springframework.stereotype.Repository;
import softeng.runtimeterror.conference.core.model.Section;

public interface SectionJpaRepository extends MyJpaRepository<Section, Long> {
}
