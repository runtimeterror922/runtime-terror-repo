package softeng.runtimeterror.conference.core.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Service;
import softeng.runtimeterror.conference.core.model.*;
import softeng.runtimeterror.conference.core.utils.ConferenceRepo;
import softeng.runtimeterror.conference.core.utils.ProposalRepo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@ComponentScan({"softeng.runtimeterror.conference.core.repository"})
public class ConferenceService {
    private static final Logger log = LoggerFactory.getLogger(ConferenceService.class);

    @Autowired
    ProposalRepo proposalRepo;
    @Autowired
    ConferenceRepo conferenceRepo;


    public void addConference(Conference conference) {
        conferenceRepo.save(conference);
        log.trace("method called -- addConference: {}", conference.toString());
    }

    public void updateConference(Long id, Conference conference) {
        conference.setId(id);
        conferenceRepo.save(conference);
    }

    public List<Conference> getAllConferences() {
        log.trace("method returned -- getAllConferences");
        return conferenceRepo.findAll();
    }

    /*public void addAuthor(Author author) {
        authorRepo.save(author);
        log.trace("method called -- addAuthor: {}", author.toString());
    }


     */

    public List<Author> getAllAuthors(Long conferenceId) {
        log.trace("method returned -- getAllAuthors");
        return new ArrayList<>(conferenceRepo.findByID(conferenceId).getAuthors());
    }


/*
    public void updateAuthor(Long id, Author author) {
        author.setId(id);
        authorRepo.save(author);
    }

    public void deleteAuthor(Long id) {
        authorRepo.deleteById(id);
    }




    public void addParticipant(Participant participant)
    {
        participantRepo.save(participant);
        log.trace("method called -- addParticipant: {}", participant.toString());
    }

    public List<Participant> getAllParticipants(){
        log.trace("method returned -- getAllParticipants");
        return participantRepo.findAll();
    }

    public void updateParticipant(Long id, Participant participant)
    {
        participant.setId(id);
        participantRepo.save(participant);
    }
    public void deleteParticipant(Long id) { participantRepo.deleteById(id);}
*/
    public void addProposal(Proposal proposal) {
        proposalRepo.save(proposal);

        //TODO add also the autor that is making the proposal to the list of authorsIds

        for (Conference conf : this.conferenceRepo.findAll()) {
            Set<Author> authors = conf.getAuthors();
            if (authors != null) {
                for (Author auth : authors) {
                    if (auth.getId() == proposal.getPresentedByAuthorId()) {
                        if (conf.getSubmittedPapersIds() == null) {
                            conf.setSubmittedPapersIds(new HashSet<>());
                        }
                        conf.getSubmittedPapersIds().add(proposal.getId());
                        System.out.println("------"+conf.getSubmittedPapersIds());
                    }
                }
            }
        }

        log.trace("method called -- addProposal: {}", proposal.toString());
    }

    public List<Proposal> getAllProposals(Long conferenceId){
        log.trace("method returned -- getAllProposals");
        List<Proposal> proposalsByConference=this.conferenceRepo.findByID(conferenceId).getSubmittedPapersIds().stream()
                .map(id->this.proposalRepo.findByID(id))
                .collect(Collectors.toList());
        return proposalsByConference;
    }

    /*
        public void updateProposal(Long id, Proposal proposal)
        {
            proposal.setId(id);
            proposalRepo.deleteById(id);
            proposalRepo.save(proposal);
        }
        public void deleteProposal(Long id) { proposalRepo.deleteById(id);}

        public void addReviewer(Reviewer reviewer)
        {
            reviewerRepo.save(reviewer);
            log.trace("method called -- addReviewer: {}", reviewer.toString());
        }
    */
    public List<Reviewer> getAllReviewers(Long conferenceId){
        log.trace("method returned -- getAllReviewers");
        return  new ArrayList<>(conferenceRepo.findByID(conferenceId).getReviewers());
    }
/*
    public void updateReviewer(Long id, Reviewer reviewer)
    {
        reviewer.setId(id);
        reviewerRepo.save(reviewer);
    }
    public void deleteReviewer(Long id) { reviewerRepo.deleteById(id);}

    public void addEvaluation(Evaluation evaluation)
    {
        evaluationRepo.save(evaluation);
        log.trace("method called -- addEvaluation: {}", evaluation.toString());
    }

    public List<Evaluation> getAllEvaluations(){
        log.trace("method returned -- getAllEvaluations");
        return evaluationRepo.findAll();
    }

    public void updateEvaluation(Long id, Evaluation evaluation)
    {
        evaluation.setId(id);
        evaluationRepo.save(evaluation);
    }
    public void deleteEvaluation(Long id) { evaluationRepo.deleteById(id);}


    public void addSection(Section section)
    {
        sectionRepo.save(section);
        log.trace("method called -- addSection: {}", section.toString());
    }

    public List<Section> getAllSections(){
        log.trace("method returned -- getAllSections");
        return sectionRepo.findAll();
    }

    public void updateSection(Long id,Section section )
    {
        section.setId(id);
        sectionRepo.save(section);
    }
    public void deleteSection(Long id) { sectionRepo.deleteById(id);}

*/

}
