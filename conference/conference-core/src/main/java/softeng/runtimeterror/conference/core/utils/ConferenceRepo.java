package softeng.runtimeterror.conference.core.utils;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;
import softeng.runtimeterror.conference.core.model.Conference;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
@Getter
@Setter
public class ConferenceRepo {
    private List<Conference> conferences;

    public ConferenceRepo() {
        this.conferences = new ArrayList<>();
    }

    public void save(Conference conf){
        conferences.add(conf);
    }

    public void delete(Long id){
        conferences = conferences.stream()
                .filter(p->p.getId()!=id)
                .collect(Collectors.toList());
    }

    public Conference findByID(Long id){
        for(Conference p: conferences){
            if(p.getId()==id)
                return p;
        }
        return null;
    }
    public List<Conference> findAll(){
        return conferences;
    }

    @Override
    public String toString() {
        return "ConferenceRepo{" + conferences +
                '}';
    }
}
