package softeng.runtimeterror.conference.core.utils;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class IDGenerator {
    private static Long count=0L;

    public Long generate(){
        count+=1;
        return count;
    }
}
