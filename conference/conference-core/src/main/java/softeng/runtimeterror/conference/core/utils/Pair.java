package softeng.runtimeterror.conference.core.utils;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Pair<A,B> implements Serializable {
    private A first;
    private B second;

    @Override
    public String toString() {
        return "(" + this.first + "," + this.second + ')';
    }
}