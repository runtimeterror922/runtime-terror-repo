package softeng.runtimeterror.conference.core.utils;

import lombok.*;
import org.springframework.stereotype.Component;
import softeng.runtimeterror.conference.core.model.Proposal;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
@Getter
@Setter
public class ProposalRepo {
    private List<Proposal> proposals;

    public ProposalRepo() {
        this.proposals = new ArrayList<>();
    }

    public void save(Proposal prop){
        proposals.add(prop);
    }

    public void delete(Long id){
        proposals=proposals.stream()
                .filter(p->p.getId()!=id)
                .collect(Collectors.toList());
    }

    public Proposal findByID(Long id){
        for(Proposal p:proposals){
            if(p.getId()==id)
                return p;
        }
        return null;
    }
    public List<Proposal> findAll(){
        return proposals;
    }

    @Override
    public String toString() {
        return "ProposalRepo{" + proposals +
                '}';
    }
}
