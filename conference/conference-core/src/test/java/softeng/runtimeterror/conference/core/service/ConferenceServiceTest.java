package softeng.runtimeterror.conference.core.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import softeng.runtimeterror.conference.core.model.Author;
import softeng.runtimeterror.conference.core.model.Conference;
import softeng.runtimeterror.conference.core.model.Proposal;
import softeng.runtimeterror.conference.core.model.Reviewer;
import softeng.runtimeterror.conference.core.utils.ConferenceRepo;
import softeng.runtimeterror.conference.core.utils.ProposalRepo;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class ConferenceServiceTest {

    private ConferenceService conferenceServiceUnderTest;
    private Conference conferenceUnderTest;
    private Proposal proposalUnderTest;
    private Reviewer reviewerUnderTest;
    private Author authorUnderTest;

    @BeforeEach
    public void setUp() {
        conferenceServiceUnderTest = new ConferenceService();
        conferenceServiceUnderTest.proposalRepo = mock(ProposalRepo.class);
        conferenceServiceUnderTest.conferenceRepo = mock(ConferenceRepo.class);

        conferenceUnderTest = new Conference(
                "ICSE 2019 : International Conference on Software Engineering",
                "CSE is the premier forum",
                new GregorianCalendar(2019, Calendar.JUNE, 4).getTime(),
                new GregorianCalendar(2019, Calendar.JUNE, 5).getTime(),
                new GregorianCalendar(2019, Calendar.JUNE, 6).getTime(),
                new GregorianCalendar(2019, Calendar.JUNE, 7).getTime(),
                new GregorianCalendar(2019, Calendar.JUNE, 8).getTime(),
                new HashSet<>(), new HashSet<>(), new HashSet<>(), new HashSet<>(), new HashSet<>());
        proposalUnderTest = new Proposal(
                "proposalTitle",
                "keywords",
                "topics",
                new HashSet<>(),
                0L,
                "abstractPaper",
                "fullPaper",
                Arrays.asList());
        reviewerUnderTest = new Reviewer(
                "firstName",
                "lastName",
                null,
                false,
                "affiliation",
                "email",
                "username",
                "password",
                "website",
                new HashSet<>(), new HashSet<>());
        authorUnderTest = new Author(
                0L,
                "firstName",
                "lastName",
                "email",
                "username",
                "password",
                false);

        Set<Reviewer> setR = new HashSet<>();
        setR.add(reviewerUnderTest);
        conferenceUnderTest.setReviewers(setR);

        Set<Author> setA = new HashSet<>();
        setA.add(authorUnderTest);
        conferenceUnderTest.setAuthors(setA);
    }

    @Test
    public void testAddConference() {
        // Setup
        final Conference conference = conferenceUnderTest;

        // Run the test
        conferenceServiceUnderTest.addConference(conferenceUnderTest);

        // Verify the results
        verify(conferenceServiceUnderTest.conferenceRepo).save(conferenceUnderTest);
    }

    @Test
    public void testUpdateConference() {
        // Setup
        final long id = 0L;
        final Conference conference = conferenceUnderTest;

        // Run the test
        conferenceServiceUnderTest.updateConference(id, conference);

        // Verify the results
        conferenceServiceUnderTest.conferenceRepo.save(conferenceUnderTest);
    }

    @Test
    public void testGetAllConferences() {
        // Setup
        final List<Conference> expectedResult = Arrays.asList(conferenceUnderTest);
        when(conferenceServiceUnderTest.conferenceRepo.findAll()).thenReturn(Arrays.asList(conferenceUnderTest));

        // Run the test
        final List<Conference> result = conferenceServiceUnderTest.getAllConferences();

        // Verify the results
        assertEquals(expectedResult, result);
    }

    @Test
    public void testGetAllAuthors() {
        // Setup
        final Long conferenceId = 0L;
        final List<Author> expectedResult = Arrays.asList(authorUnderTest);
        when(conferenceServiceUnderTest.conferenceRepo.findByID(0L)).thenReturn(conferenceUnderTest);

        // Run the test
        final List<Author> result = conferenceServiceUnderTest.getAllAuthors(conferenceId);

        // Verify the results
        assertEquals(expectedResult, result);
    }

    @Test
    public void testAddProposal() {
        // Setup
        final Proposal proposal = proposalUnderTest;
        when(conferenceServiceUnderTest.conferenceRepo.findAll()).thenReturn(Arrays.asList());

        // Run the test
        conferenceServiceUnderTest.addProposal(proposal);

        // Verify the results
        verify(conferenceServiceUnderTest.proposalRepo).save(proposalUnderTest);
        conferenceServiceUnderTest.proposalRepo.save(proposalUnderTest);
    }

    @Test
    public void testGetAllProposals() {
        // Setup
        final Long conferenceId = 0L;
        final List<Proposal> expectedResult = Arrays.asList();
        when(conferenceServiceUnderTest.conferenceRepo.findByID((long)0)).thenReturn(conferenceUnderTest);
        when(conferenceServiceUnderTest.proposalRepo.findByID((long)0)).thenReturn(proposalUnderTest);

        // Run the test
        final List<Proposal> result = conferenceServiceUnderTest.getAllProposals(conferenceId);

        // Verify the results
        conferenceServiceUnderTest.proposalRepo.save(proposalUnderTest);
        assertEquals(expectedResult, result);
    }

    @Test
    public void testGetAllReviewers() {
        // Setup
        final Long conferenceId = 0L;
        final List<Reviewer> expectedResult = Arrays.asList(reviewerUnderTest);
        when(conferenceServiceUnderTest.conferenceRepo.findByID(0L)).thenReturn(conferenceUnderTest);

        // Run the test
        final List<Reviewer> result = conferenceServiceUnderTest.getAllReviewers(conferenceId);

        // Verify the results
        assertEquals(expectedResult, result);
    }
}
