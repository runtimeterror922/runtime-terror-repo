package softeng.runtimeterror.conference.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import softeng.runtimeterror.conference.core.model.*;
import softeng.runtimeterror.conference.core.service.ConferenceService;
import softeng.runtimeterror.conference.core.utils.IDGenerator;
import softeng.runtimeterror.conference.web.converter.*;
import softeng.runtimeterror.conference.web.dto.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
public class ConferenceController {
    private static final Logger log = LoggerFactory.getLogger(ConferenceController.class);

    private final ConferenceService service;
    private final IDGenerator idGen;

    @Autowired
    private ConferenceConverter conferenceConverter;
    @Autowired
    private AuthorConverter authorConverter;
    @Autowired
    private ParticipantConverter participantConverter;
    @Autowired
    private ProposalConverter proposalConverter;
    @Autowired
    private ReviewerConverter reviewerConverter;
    @Autowired
    private EvaluationConverter evaluationConverter;
    @Autowired
    private SectionConverter sectionConverter;

    @Autowired
    public ConferenceController(ConferenceService service, IDGenerator idGen ) {
        this.service = service;
        this.idGen=idGen;
        this.addSome();
    }

    @RequestMapping(path = "/conferences", method = RequestMethod.GET)
    List<ConferenceDTO> getAllConferences() {
        log.trace("method called -- getAllConferences");
        List<Conference> conferences = service.getAllConferences();

        log.trace("method returned -- getAllConferences: {}", conferences);
        return new ArrayList<>(conferenceConverter.convertModelsToDtos(conferences));
    }

    @RequestMapping(path = "/conferences", method = RequestMethod.POST)
    void saveConference(@RequestBody ConferenceDTO dto) {
        log.trace("method called -- saveConference: {}", dto);
        this.service.addConference(conferenceConverter.convertDtoToModel(dto));
        log.trace("method returned -- saveConference");
    }


    @RequestMapping(value = "/authors/{conferenceId}", method = RequestMethod.GET)
    List<AuthorDTO> getAllAuthors(@PathVariable Long conferenceId) {
        log.trace("method called -- getAllAuthors");
        List<Author> authors = service.getAllAuthors(conferenceId);

        log.trace("method returned -- getAllAuthors: {}", authors);
        return new ArrayList<>(authorConverter.convertModelsToDtos(authors));
    }
    /*
        @RequestMapping(value = "/authors", method = RequestMethod.POST)
        void saveAuthor(@RequestBody AuthorDTO dto) {
            log.trace("method called -- saveAuthor: {}", dto);
            this.service.addAuthor(authorConverter.convertDtoToModel(dto));
            log.trace("method returned -- saveAuthor");
        }


        @RequestMapping(value = "/authors/{id}", method = RequestMethod.PUT)
        void updateAuthor(@PathVariable Long id, @RequestBody AuthorDTO dto) {
            log.trace("updateAuthor: id={},dto={}", id, dto);
            service.updateAuthor(id, authorConverter.convertDtoToModel(dto));
            log.trace("updateAuthor finished");
        }

        @RequestMapping(value = "/authors/{id}", method = RequestMethod.DELETE)
        void deleteAuthor(@PathVariable Long id) {
            log.trace("deleteAuthor: id={}", id);
            service.deleteAuthor(id);
            log.trace("deleteAuthor --- method finished");
        }



        @RequestMapping(value = "/participants", method = RequestMethod.GET)
        List<ParticipantDTO> getAllParticipants() {
            log.trace("method called -- getAllParticipants");
            List<Participant> participants = service.getAllParticipants();

            log.trace("method returned -- getAllParticipants: {}", participants);
            return new ArrayList<>(participantConverter.convertModelsToDtos(participants));
        }

        @RequestMapping(value = "/participants", method = RequestMethod.POST)
        void saveParticipant(@RequestBody ParticipantDTO dto) {
            log.trace("method called -- saveParticipant: {}", dto);
            this.service.addParticipant(participantConverter.convertDtoToModel(dto));
            log.trace("method returned -- saveParticipant");
        }


        @RequestMapping(value = "/participants/{id}", method = RequestMethod.PUT)
        void updateParticipant(@PathVariable Long id, @RequestBody ParticipantDTO dto) {
            log.trace("updateParticipant: id={},dto={}", id, dto);
            service.updateParticipant(id, participantConverter.convertDtoToModel(dto));
            log.trace("updateParticipant finished");
        }

        @RequestMapping(value = "/participants/{id}", method = RequestMethod.DELETE)
        void deleteParticipant(@PathVariable Long id) {
            log.trace("deleteParticipant: id={}", id);
            service.deleteParticipant(id);
            log.trace("deleteParticipant--- method finished");
        }

*/

    @RequestMapping(value = "/proposals/{conferenceId}", method = RequestMethod.GET)
    List<ProposalDTO> getAllProposals(@PathVariable Long conferenceId) {
        log.trace("method called -- getAllProposals");
        List<Proposal> proposals = service.getAllProposals(conferenceId);

        log.trace("method returned -- getAllProposals: {}", proposals);
        return new ArrayList<>(proposalConverter.convertModelsToDtos(proposals));
    }

    @RequestMapping(path = "/proposals", method = RequestMethod.POST)
    void saveProposal(@RequestBody ProposalDTO dto) {
        log.trace("method called -- saveProposal: {}", dto);
        this.service.addProposal(proposalConverter.convertDtoToModel(dto));

        log.trace("method returned -- saveProposal");
    }

/*
    @RequestMapping(value = "/conferences/{id}", method = RequestMethod.PUT)
    void updateProposal(@PathVariable Long id, @RequestBody ProposalDTO dto) {
        log.trace("updateProposal: id={},dto={}", id, dto);
        service.updateProposal(id, proposalConverter.convertDtoToModel(dto));
        log.trace("updateProposal finished");
    }

    @RequestMapping(value = "/conferences/{id}", method = RequestMethod.DELETE)
    void deleteProposal(@PathVariable Long id) {
        log.trace("deleteProposal: id={}", id);
        service.deleteProposal(id);
        log.trace("deleteProposal --- method finished");
    }



/*/

    @RequestMapping(value = "/reviewers/{conferenceId}", method = RequestMethod.GET)
    List<ReviewerDTO> getAllReviewers(@PathVariable Long conferenceId) {
        log.trace("method called -- getAllReviewers");
        List<Reviewer> reviewers = service.getAllReviewers(conferenceId);

        log.trace("method returned -- getAllReviewers: {}", reviewers);
        return new ArrayList<>(reviewerConverter.convertModelsToDtos(reviewers));
    }

    /*
        @RequestMapping(value = "/reviewers", method = RequestMethod.POST)
        void saveReviewer(@RequestBody ReviewerDTO dto) {
            log.trace("method called -- saveReviewer: {}", dto);
            this.service.addReviewer(reviewerConverter.convertDtoToModel(dto));
            log.trace("method returned -- saveReviewer");
        }


        @RequestMapping(value = "/reviewers/{id}", method = RequestMethod.PUT)
        void updateReviewer(@PathVariable Long id, @RequestBody ReviewerDTO dto) {
            log.trace("updateReviewer: id={},dto={}", id, dto);
            service.updateReviewer(id, reviewerConverter.convertDtoToModel(dto));
            log.trace("updateReviewer finished");
        }

        @RequestMapping(value = "/reviewers/{id}", method = RequestMethod.DELETE)
        void deleteReviewer(@PathVariable Long id) {
            log.trace("deleteReviewer: id={}", id);
            service.deleteReviewer(id);
            log.trace("deleteReviewer --- method finished");
        }


    @RequestMapping(value = "/evaluations", method = RequestMethod.GET)
    List<EvaluationDTO> getAllEvaluations() {
        log.trace("method called -- getAllEvaluations");
        List<Evaluation> evaluations = service.getAllEvaluations();

        log.trace("method returned -- getAllEvaluations: {}", evaluations);
        return new ArrayList<>(evaluationConverter.convertModelsToDtos(evaluations));
    }

        @RequestMapping(value = "/evaluations", method = RequestMethod.POST)
        void saveEvaluation(@RequestBody EvaluationDTO dto) {
            log.trace("method called -- saveEvaluation: {}", dto);
            this.service.addEvaluation(evaluationConverter.convertDtoToModel(dto));
            log.trace("method returned -- saveReviewer");
        }


        @RequestMapping(value = "/evaluations/{id}", method = RequestMethod.PUT)
        void updateEvaluation(@PathVariable Long id, @RequestBody EvaluationDTO dto) {
            log.trace("updateEvaluation: id={},dto={}", id, dto);
            service.updateEvaluation(id, evaluationConverter.convertDtoToModel(dto));
            log.trace("updateEvaluation finished");
        }

        @RequestMapping(value = "/evaluations/{id}", method = RequestMethod.DELETE)
        void deleteEvaluation(@PathVariable Long id) {
            log.trace("deleteEvaluation: id={}", id);
            service.deleteEvaluation(id);
            log.trace("deleteEvaluation --- method finished");
        }


        @RequestMapping(value = "/sections", method = RequestMethod.GET)
        List<SectionDTO> getAllSections() {
            log.trace("method called -- getAllEvaluations");
            List<Section> sections = service.getAllSections();

            log.trace("method returned -- getAllSections: {}", sections);
            return new ArrayList<>(sectionConverter.convertModelsToDtos(sections));
        }

        @RequestMapping(value = "/sections", method = RequestMethod.POST)
        void saveSection(@RequestBody SectionDTO dto) {
            log.trace("method called -- saveSection: {}", dto);
            this.service.addSection(sectionConverter.convertDtoToModel(dto));
            log.trace("method returned -- saveSection");
        }


        @RequestMapping(value = "/sections/{id}", method = RequestMethod.PUT)
        void updateSection(@PathVariable Long id, @RequestBody SectionDTO dto) {
            log.trace("updateSection: id={},dto={}", id, dto);
            service.updateSection(id, sectionConverter.convertDtoToModel(dto));
            log.trace("updateSection finished");
        }

        @RequestMapping(value = "/sections/{id}", method = RequestMethod.DELETE)
        void deleteSection(@PathVariable Long id) {
            log.trace("deleteSection: id={}", id);
            service.deleteSection(id);
            log.trace("deleteSection--- method finished");
        }
    */
    private void addSome() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

        try {
            System.out.println("yeeeeha");
            log.trace("yeeeehaaa");
            Set<Author> authors = new HashSet<>();


            authors.add(new Author(1L, "Martin", "Luther", "luther@gmail.com", "lutherino", "1234abcd", false));
            authors.add(new Author(2L, "Tom", "Harris", "harrisharr@gmail.com", "harrrr", "1234abcda", false));
            authors.add(new Author(3L, "John", "Johnas", "johnas@johnas.com", "jjj", "1234abcdb", false));
            authors.add(new Author(4L, "Robert", "Baratheon", "robert.just@gmail.com", "robmob", "1234abcdc", false));


            Set<Reviewer> reviewers = new HashSet<>();
            Reviewer rev1 = Reviewer.builder()
                    .firstName("Dobrea")
                    .lastName("Andrei")
                    .email("dobreaandrei98@gmail.com")
                    .username("andy")
                    .password("andy")
                    .affiliation("ultraStudent")
                    .website("https://www.facebook.com/andrei.dobrea.37")
                    .type(Reviewer.Type.PCMEMBER)
                    .wouldEvaluateProposals(new HashSet<>())
                    .refuseEvaluateProposals(new HashSet<>())
                    .build();
            rev1.setId(1L);

            Reviewer rev2 = Reviewer.builder()
                    .firstName("Enrique")
                    .lastName("Iglesias")
                    .email("enrique@bbc.uk")
                    .affiliation("popstar")
                    .website("https://www.enriqueiglesias.com/")
                    .username("enrique")
                    .password("enrique")
                    .type(Reviewer.Type.PCMEMBER)
                    .wouldEvaluateProposals(new HashSet<>())
                    .refuseEvaluateProposals(new HashSet<>())
                    .build();
            rev2.setId(2L);

            Reviewer rev3 = Reviewer.builder()
                    .firstName("Andy")
                    .lastName("Moisescu")
                    .email("andy@noreply.org")
                    .affiliation("journalist")
                    .website("http://andimoisescu.ro/despre-mine/")
                    .username("andym")
                    .password("andym")
                    .type(Reviewer.Type.PCMEMBER)
                    .wouldEvaluateProposals(new HashSet<>())
                    .refuseEvaluateProposals(new HashSet<>())
                    .build();
            rev3.setId(3L);

            reviewers.add(rev1);
            reviewers.add(rev2);
            reviewers.add(rev3);

            Conference c1 = Conference.builder()
                    .name("ICSE 2019 : International Conference on Software Engineering")
                    .callForPapers("CSE is the premier forum for presenting and discussing the most recent and significant technical research contributions in the field of Software Engineering. We invite high quality submissions of technical research papers describing original and unpublished results of software engineering research. We welcome submissions addressing topics across the full spectrum of Software Engineering.")
                    .startingTime(simpleDateFormat.parse("2019-09-16"))
                    .endingTime(simpleDateFormat.parse("2019-09-19"))
                    .abstractDeadline(simpleDateFormat.parse("2019-08-08"))
                    .fullpaperDeadline(simpleDateFormat.parse("2019-09-09"))
                    .biddingDeadline(simpleDateFormat.parse("2019-10-10"))
                    .authors(authors)
                    .build();

            Conference c2 = Conference.builder()
                    .name("SP 2019 : IEEE Conference on Security and Privacy")
                    .callForPapers("Since 1980 in Oakland, the IEEE Conference on Security and Privacy has been the premier forum for computer security research, presenting the latest developments and bringing together researchers and practitioners. We solicit previously unpublished papers offering novel research contributions in any aspect of security or privacy. Papers may present advances in the theory, design, implementation, analysis, verification, or empirical evaluation and measurement of secure systems.")
                    .startingTime(simpleDateFormat.parse("2019-04-06"))
                    .endingTime(simpleDateFormat.parse("2019-04-07"))
                    .abstractDeadline(simpleDateFormat.parse("2019-03-03"))
                    .fullpaperDeadline(simpleDateFormat.parse("2019-04-04"))
                    .biddingDeadline(simpleDateFormat.parse("2019-05-05"))
                    .build();

            Conference c3 = Conference.builder()
                    .name("ICASSP 2019 : IEEE International Conference on Acoustics, Speech and Signal Processing (ICASSP)")
                    .callForPapers("ICASSP is the foremost and most comprehensive international conference on signal processing and its applications. The 44th edition of ICASSP will be held in one of the ten most beautiful coastal cities in the world, Brighton, which is famous for its pebble beach and vibrant city life.\n\nThe programme will include keynotes by pre-eminent international speakers, cutting-edge tutorial topics, and forward looking special sessions. ICASSP also provides a great networking opportunity with a wide range of like-minded professionals from academia, industry and government organizations. The theme of the 44th ICASSP is Signal Processing: Empowering Science and Technology for Humankind.")
                    .startingTime(simpleDateFormat.parse("2020-09-16"))
                    .endingTime(simpleDateFormat.parse("2020-09-19"))
                    .abstractDeadline(simpleDateFormat.parse("2020-08-08"))
                    .fullpaperDeadline(simpleDateFormat.parse("2020-09-09"))
                    .biddingDeadline(simpleDateFormat.parse("2020-10-10"))
                    .build();

            Conference c4 = Conference.builder()
                    .name("International Conference on Research in Social Sciences")
                    .callForPapers("A leading conference in politics, humanities, communications, law, education, and a number of other areas of social sciences, RSSCONF is coming to Rome, Italy this year on the 10th-12th of May. Bringing together 250 top researchers, scientists, and other members of the academia, this premier academic event will dive into some of the most pressing issues in the industry, looking at practical examples and coming up with creative solutions.\n\nThe agenda features dozens of topics to pick from, providing you with the perfect platform to share your knowledge, learn from your peers, and network with the leading minds of the industry.")
                    .startingTime(simpleDateFormat.parse("2019-11-11"))
                    .endingTime(simpleDateFormat.parse("2019-11-16"))
                    .abstractDeadline(simpleDateFormat.parse("2019-07-03"))
                    .fullpaperDeadline(simpleDateFormat.parse("2019-08-04"))
                    .biddingDeadline(simpleDateFormat.parse("2019-07-15"))
                    .build();

            Set<Long> authorIds = new HashSet<>();
            authorIds.add(1L);
            authorIds.add(2L);


            Proposal prop1 = Proposal.builder()
                    .proposalTitle("Deviation of particles")
                    .keywords("particles, deviation")
                    .topics("physics, atom-theory")
                    .authorIds(authorIds)
                    .presentedByAuthorId(1L)
                    .abstractPaper("dev_part1.pdf")
                    .fullPaper("deviation_particles.ppt")
                    .build();
            prop1.setId(idGen.generate());

            Proposal prop2 = Proposal.builder()
                    .proposalTitle("Black holes rediscovered")
                    .keywords("black, hole, space")
                    .topics("physisc, space")
                    .authorIds(authorIds)
                    .presentedByAuthorId(1L)
                    .abstractPaper("future_travel_beta.pdf")
                    .fullPaper("future_travel_finished.pdf")
                    .build();
            prop2.setId(idGen.generate());

            Proposal prop3 = Proposal.builder()
                    .proposalTitle("How to sell anything")
                    .keywords("marketing, ultimate, now")
                    .topics("economics, money")
                    .authorIds(authorIds)
                    .presentedByAuthorId(1L)
                    .abstractPaper("BESTPRESENTATION.pdf")
                    .fullPaper("BESTPRESENTATION.pdf")
                    .build();
            prop3.setId(idGen.generate());

            Proposal prop4 = Proposal.builder()
                    .proposalTitle("How to buy anything cheap")
                    .keywords("marketing, ultimate, now")
                    .topics("economics, money")
                    .authorIds(authorIds)
                    .presentedByAuthorId(1L)
                    .abstractPaper("BESTPRESENTATION_VERSION2.pdf")
                    .fullPaper("BESTPRESENTATION_VERSION2.pdf")
                    .build();
            prop4.setId(idGen.generate());

            Proposal prop5 = Proposal.builder()
                    .proposalTitle("Quantum matter in personal relationships")
                    .keywords("quantum, matters, you")
                    .topics("love, quantum")
                    .authorIds(authorIds)
                    .presentedByAuthorId(1L)
                    .abstractPaper("quantum_is_love.pdf")
                    .fullPaper("quantum_is_hate.pdf")
                    .build();
            prop5.setId(idGen.generate());

            service.addProposal(prop1);
            service.addProposal(prop2);
            service.addProposal(prop3);
            service.addProposal(prop4);
            service.addProposal(prop5);

            c1.setId(1L);
            c1.setReviewers(reviewers);
            c1.setSubmittedPapersIds(new HashSet<>());
            c1.getSubmittedPapersIds().add(prop1.getId());
            c1.getSubmittedPapersIds().add(prop2.getId());
            c1.getSubmittedPapersIds().add(prop3.getId());
            c1.getSubmittedPapersIds().add(prop4.getId());
            c1.getSubmittedPapersIds().add(prop5.getId());


            c2.setId(2L);
            c3.setId(3L);
            c4.setId(4L);

            service.addConference(c1);
            service.addConference(c2);
            service.addConference(c3);
            service.addConference(c4);
            System.out.println("yeeeeha");
            log.trace("yeeeehaaa");

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
