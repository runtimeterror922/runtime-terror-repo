package softeng.runtimeterror.conference.web.converter;

import org.springframework.stereotype.Component;
import softeng.runtimeterror.conference.core.model.Author;
import softeng.runtimeterror.conference.web.dto.AuthorDTO;

@Component
public class AuthorConverter extends BaseConverter<Author, AuthorDTO> {
    @Override
    public Author convertDtoToModel(AuthorDTO dto) {
        /*Author author = Author.builder()
                .firstName(dto.getFirstName())
                .lastName(dto.getLastName())
                .email(dto.getEmail())
                .username(dto.getUsername())
                .password(dto.getPassword())
                .validated(dto.getValidated())
                .build();
        author.setId(dto.getId());
        return author;*/
        return null;
    }

    @Override
    public AuthorDTO convertModelToDto(Author author) {
        AuthorDTO dto = AuthorDTO.builder()
                .firstName(author.getFirstName())
                .lastName(author.getLastName())
                .email(author.getEmail())
                .username(author.getUsername())
                .password(author.getPassword())
                .validated(author.getValidated())
                .build();
        dto.setId(author.getId());
        return dto;
    }
}
