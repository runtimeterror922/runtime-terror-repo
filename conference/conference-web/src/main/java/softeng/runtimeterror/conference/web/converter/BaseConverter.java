package softeng.runtimeterror.conference.web.converter;

import softeng.runtimeterror.conference.core.model.AbstractModel;
import softeng.runtimeterror.conference.web.dto.BaseDTO;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

public abstract class BaseConverter<Model extends AbstractModel<Long>, DTO extends BaseDTO> implements Converter<Model, DTO> {

    public Set<Long> convertModelsToIDs(Set<Model> models) {
        return models.stream()
                .map(AbstractModel::getId)
                .collect(Collectors.toSet());
    }

    public Set<Long> convertDTOsToIDs(Set<DTO> dtos) {
        return dtos.stream()
                .map(BaseDTO::getId)
                .collect(Collectors.toSet());
    }

    public Set<DTO> convertModelsToDtos(Collection<Model> models) {
        return models.stream()
                .map(this::convertModelToDto)
                .collect(Collectors.toSet());
    }
}
