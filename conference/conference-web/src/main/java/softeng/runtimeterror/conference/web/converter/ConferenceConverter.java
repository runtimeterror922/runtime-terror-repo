package softeng.runtimeterror.conference.web.converter;

import org.springframework.stereotype.Component;
import softeng.runtimeterror.conference.core.model.Conference;
import softeng.runtimeterror.conference.web.dto.ConferenceDTO;

@Component
public class ConferenceConverter extends BaseConverter<Conference, ConferenceDTO> {
    @Override
    public Conference convertDtoToModel(ConferenceDTO dto) {
        Conference conference = Conference.builder()
                .name(dto.getName())
                .callForPapers(dto.getCallForPapers())
                .startingTime(dto.getStartingTime())
                .endingTime(dto.getEndingTime())
                .abstractDeadline(dto.getAbstractDeadline())
                .fullpaperDeadline(dto.getFullpaperDeadline())
                .biddingDeadline(dto.getBiddingDeadline())
                .build();
        conference.setId(dto.getId());
        return conference;
    }

    @Override
    public ConferenceDTO convertModelToDto(Conference conference) {
        ConferenceDTO dto = ConferenceDTO.builder()
                .name(conference.getName())
                .callForPapers(conference.getCallForPapers())
                .startingTime(conference.getStartingTime())
                .endingTime(conference.getEndingTime())
                .abstractDeadline(conference.getAbstractDeadline())
                .fullpaperDeadline(conference.getFullpaperDeadline())
                .biddingDeadline(conference.getBiddingDeadline())
                .build();
        dto.setId(conference.getId());
        return dto;
    }
}
