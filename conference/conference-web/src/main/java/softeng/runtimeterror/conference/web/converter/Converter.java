package softeng.runtimeterror.conference.web.converter;

import softeng.runtimeterror.conference.core.model.AbstractModel;
import softeng.runtimeterror.conference.web.dto.BaseDTO;

public interface Converter<Model extends AbstractModel<Long>, Dto extends BaseDTO> {

    Model convertDtoToModel(Dto dto);

    Dto convertModelToDto(Model model);
}
