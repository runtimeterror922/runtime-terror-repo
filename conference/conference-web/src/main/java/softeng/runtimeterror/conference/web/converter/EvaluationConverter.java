package softeng.runtimeterror.conference.web.converter;

import org.springframework.stereotype.Component;
import softeng.runtimeterror.conference.core.model.Evaluation;
import softeng.runtimeterror.conference.web.dto.EvaluationDTO;


@Component
public class EvaluationConverter extends BaseConverter<Evaluation, EvaluationDTO> {
    @Override
    public Evaluation convertDtoToModel(EvaluationDTO dto) {
        Evaluation evaluation = Evaluation.builder()
                .accepted(dto.getAccepted())
                .mark(dto.getMark())
                .suggestion(dto.getSuggestion())
                .build();
        evaluation.setId(dto.getId());
        return evaluation;
    }

    @Override
    public EvaluationDTO convertModelToDto(Evaluation evaluation) {
        EvaluationDTO dto = EvaluationDTO.builder()
                .accepted(evaluation.getAccepted())
                .suggestion(evaluation.getSuggestion())
                .mark(evaluation.getMark())
                .build();
        dto.setId(evaluation.getId());
        return dto;
    }
}
