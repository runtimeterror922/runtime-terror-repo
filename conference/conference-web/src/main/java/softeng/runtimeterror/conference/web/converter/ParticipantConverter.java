package softeng.runtimeterror.conference.web.converter;

import org.springframework.stereotype.Component;
import softeng.runtimeterror.conference.core.model.AbstractModel;
import softeng.runtimeterror.conference.core.model.Participant;
import softeng.runtimeterror.conference.web.dto.BaseDTO;
import softeng.runtimeterror.conference.web.dto.ParticipantDTO;

@Component
public class ParticipantConverter extends BaseConverter<Participant, ParticipantDTO> {

    public Participant convertDtoToModel(ParticipantDTO dto)
    {
        Participant participant = Participant.builder().
                firstName(dto.getFirstName()).
                lastName(dto.getLastName()).
                payed(dto.getPayed()).
                steeringCommitteeMember(dto.getSteeringCommitteeMember()).
                build();
        participant.setId(dto.getId());
        return participant;
    }

    @Override
    public ParticipantDTO convertModelToDto(Participant participant) {
        ParticipantDTO dto = ParticipantDTO.builder().
                firstName(participant.getFirstName()).
                lastName(participant.getLastName()).
                payed(participant.getPayed()).
                steeringCommitteeMember(participant.getSteeringCommitteeMember()).
                build();
        dto.setId(participant.getId());
        return dto;
    }
}
