package softeng.runtimeterror.conference.web.converter;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import softeng.runtimeterror.conference.core.utils.IDGenerator;
import softeng.runtimeterror.conference.core.model.Proposal;
import softeng.runtimeterror.conference.web.dto.ProposalDTO;

import java.util.HashSet;
import java.util.stream.Collectors;

@Getter
@Setter
@Component
public class ProposalConverter extends BaseConverter<Proposal, ProposalDTO> {

    @Autowired
    IDGenerator idGen;

    @Override
    public Proposal convertDtoToModel(ProposalDTO dto) {
        Proposal proposal = Proposal.builder()
                .proposalTitle(dto.getProposalTitle())
                .abstractPaper(dto.getAbstractPaper())
                .fullPaper(dto.getFullPaper())
                .keywords(dto.getKeywords())
                .topics(dto.getTopics())
                .authorIds(new HashSet<>(dto.getAuthorsIds().stream()
                    .map(id->Long.parseLong(id))
                    .collect(Collectors.toList())))
                .presentedByAuthorId(Long.parseLong(dto.getPresentingAuthorId()))
                .build();
        proposal.setId(idGen.generate());
        return proposal;
    }

    @Override
    public ProposalDTO convertModelToDto(Proposal proposal) {
        ProposalDTO dto = ProposalDTO.builder()
                .abstractPaper(proposal.getAbstractPaper())
                .fullPaper(proposal.getFullPaper())
                .keywords(proposal.getKeywords())
                .proposalTitle(proposal.getProposalTitle())
                .topics(proposal.getTopics())
                .authorsIds(proposal.getAuthorIds().stream()
                    .map(id->id.toString())
                    .collect(Collectors.toList()))
                .presentingAuthorId(proposal.getPresentedByAuthorId().toString())
                .build();
        dto.setId(proposal.getId());
        return dto;
    }
}
