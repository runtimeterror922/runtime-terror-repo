package softeng.runtimeterror.conference.web.converter;

import org.springframework.stereotype.Component;
import softeng.runtimeterror.conference.core.model.Reviewer;
import softeng.runtimeterror.conference.web.dto.ReviewerDTO;


@Component
public class ReviewerConverter extends BaseConverter<Reviewer, ReviewerDTO> {
    @Override
    public Reviewer convertDtoToModel(ReviewerDTO dto) {
        Reviewer reviewer = Reviewer.builder()
                .affiliation(dto.getAffiliation())
                .email(dto.getEmail())
                .firstName(dto.getFirstName())
                .isDeadlineResponsible(dto.getIsDeadlineResponsible())
                .lastName(dto.getLastName())
                .password(dto.getPassword())
                .type(dto.getType())
                .username(dto.getUsername())
                .website(dto.getWebsite())
                .build();
        reviewer.setId(dto.getId());
        return reviewer;
    }

    @Override
    public ReviewerDTO convertModelToDto(Reviewer reviewer) {
        ReviewerDTO dto = ReviewerDTO.builder()
                .affiliation(reviewer.getAffiliation())
                .email(reviewer.getEmail())
                .firstName(reviewer.getFirstName())
                .isDeadlineResponsible(reviewer.getIsDeadlineResponsible())
                .lastName(reviewer.getLastName())
                .password(reviewer.getPassword())
                .type(reviewer.getType())
                .username(reviewer.getUsername())
                .website(reviewer.getWebsite())
                .build();
        dto.setId(reviewer.getId());
        return dto;
    }
}
