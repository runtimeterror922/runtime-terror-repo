package softeng.runtimeterror.conference.web.converter;

import org.springframework.stereotype.Component;
import softeng.runtimeterror.conference.core.model.Section;
import softeng.runtimeterror.conference.web.dto.SectionDTO;


@Component
public class SectionConverter extends BaseConverter<Section, SectionDTO> {
    @Override
    public Section convertDtoToModel(SectionDTO dto) {
        Section section = Section.builder()
                .room(dto.getRoom())
                .build();
        section.setId(dto.getId());
        return section;
    }

    @Override
    public SectionDTO convertModelToDto(Section section) {
        SectionDTO dto = SectionDTO.builder()
                .room(section.getRoom())
                .build();
        dto.setId(section.getId());
        return dto;
    }
}
