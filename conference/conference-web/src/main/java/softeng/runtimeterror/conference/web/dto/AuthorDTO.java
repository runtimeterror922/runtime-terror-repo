package softeng.runtimeterror.conference.web.dto;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Builder
public class AuthorDTO extends BaseDTO {
    private String firstName;
    private String lastName;
    private String email;
    private String username;
    private String password;
    private Boolean validated;
}
