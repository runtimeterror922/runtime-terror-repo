package softeng.runtimeterror.conference.web.dto;

import lombok.*;

import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Builder
public class ConferenceDTO extends BaseDTO {
    private String name;
    private String callForPapers;
    private Date startingTime;
    private Date endingTime;
    private Date abstractDeadline;
    private Date fullpaperDeadline;
    private Date biddingDeadline;
}
