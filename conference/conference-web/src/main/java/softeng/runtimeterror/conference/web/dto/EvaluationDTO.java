package softeng.runtimeterror.conference.web.dto;


import lombok.*;
import softeng.runtimeterror.conference.core.model.Evaluation;

@NoArgsConstructor
@AllArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Builder
public class EvaluationDTO extends BaseDTO {
    private Evaluation.Mark mark;
    private String suggestion;
    private Boolean accepted;
}
