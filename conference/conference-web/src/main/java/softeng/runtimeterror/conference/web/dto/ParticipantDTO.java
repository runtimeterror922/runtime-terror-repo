package softeng.runtimeterror.conference.web.dto;


import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Builder
public class ParticipantDTO extends BaseDTO {
    private String firstName;
    private String lastName;
    private Boolean payed;
    private Boolean steeringCommitteeMember;
}
