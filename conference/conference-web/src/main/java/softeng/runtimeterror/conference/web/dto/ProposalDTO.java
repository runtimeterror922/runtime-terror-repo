package softeng.runtimeterror.conference.web.dto;


import com.sun.xml.bind.v2.model.core.ID;
import lombok.*;
import softeng.runtimeterror.conference.core.model.Author;

import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import java.util.List;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Builder
public class ProposalDTO extends BaseDTO {
    private String proposalTitle;
    private String keywords;
    private String topics;
    private List<String> authorsIds;
    private String presentingAuthorId;
    private String abstractPaper; // TODO later this needs to be a file somehow
    private String fullPaper; // TODO later this needs to be a file somehow
}
