package softeng.runtimeterror.conference.web.dto;


import lombok.*;
import softeng.runtimeterror.conference.core.model.Reviewer;

@NoArgsConstructor
@AllArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Builder
public class ReviewerDTO extends BaseDTO {
    private String firstName;
    private String lastName;
    private Reviewer.Type type;
    private Boolean isDeadlineResponsible;
    private String affiliation;
    private String email;
    private String username;
    private String password;
    private String website;
}
