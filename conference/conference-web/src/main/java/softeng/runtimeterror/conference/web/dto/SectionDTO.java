package softeng.runtimeterror.conference.web.dto;


import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Builder
public class SectionDTO extends BaseDTO {
    private String room;
}
