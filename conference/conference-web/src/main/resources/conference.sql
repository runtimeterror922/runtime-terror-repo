CREATE DATABASE conference_ms
GO

USE conference_ms
GO

CREATE LOGIN conference_user WITH PASSWORD = 'password'
GO

IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'conference_user')
BEGIN
    CREATE USER [conference_user] FOR LOGIN [conference_user]
    EXEC sp_addrolemember N'db_owner', N'conference_user'
END
GO