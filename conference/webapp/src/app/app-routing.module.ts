import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {IndexComponent} from "./index/index.component";

import {AuthorListComponent} from "./authors/author-list/author-list.component";
import {AuthorUpdateComponent} from "./authors/author-update/author-update.component";
import {AuthorNewComponent} from "./authors/author-new/author-new.component";
import {ConferenceDetailComponent} from "./index/conference-detail/conference-detail.component";
import {AboutComponent} from "./about/about.component";
import {ConferenceNewComponent} from "./index/conference-new/conference-new.component";
import {RegisterReviewerComponent} from "./index/register-reviewer/register-reviewer.component";
import {RegisterAuthorComponent} from "./index/register-author/register-author.component";
import {RegisterListenerComponent} from "./index/register-listener/register-listener.component";
import {LoginComponent} from "./login/login.component";
import {LoginReviewerComponent} from "./login-reviewer-dobrea/login-reviewer.component";
import {ReviewerDashboardComponent} from "./reviewer-dashboard-dobrea/reviewer-dashboard.component"
import {DashboardAuthorComponent} from "./dashboard-author/dashboard-author.component";
import {DashboardReviewerBiddingComponent} from "./dashboard-reviewer-bidding/dashboard-reviewer-bidding.component";
import {DashboardReviewerEvaluationComponent} from "./dashboard-reviewer-evaluation/dashboard-reviewer-evaluation.component";
import {ListenerDashboard} from "./listener.dashboard/listener.dashboard.component";

const routes: Routes = [
    {path: 'chair/list-personnel', component: AuthorListComponent},
    {path: 'chair/update-author/:id', component: AuthorUpdateComponent},

    {path: 'index', component: IndexComponent},
    {path: 'conferences/detail/:id', component: ConferenceDetailComponent},
    {path: 'conferences/new', component: ConferenceNewComponent},

    {path: 'conferences/reg-reviewer/:id', component: RegisterReviewerComponent},
    {path: 'conferences/reg-author/:id', component: RegisterAuthorComponent},
    {path: 'conferences/reg-listener/:id', component: RegisterListenerComponent},

    {path: 'dashboard/author', component: DashboardAuthorComponent},
    {path: 'dashboard/reviewer-bidding', component: DashboardReviewerBiddingComponent},
    {path: 'dashboard/reviewer-evaluation', component: DashboardReviewerEvaluationComponent},

    {path: 'login', component: LoginComponent},
    {path: 'about', component: AboutComponent},


    // {path: 'student/detail/:id', component: StudentDetailComponent},
    // {path: 'student-new', component: StudentNewComponent},

    {path: 'login/:conferenceId/:loginType', component: LoginReviewerComponent},
    {path: 'reviewerDashboard/:conferenceId', component: ReviewerDashboardComponent},
    {path: 'dashboard/listener', component: ListenerDashboard},
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
