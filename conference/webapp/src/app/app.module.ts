import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { AuthorsComponent } from './authors/authors.component';
import { AuthorService } from "./shared/author.service";
import { AuthorListComponent } from './authors/author-list/author-list.component';
import { AuthorDetailComponent } from './authors/author-detail/author-detail.component';
import { AuthorNewComponent } from './authors/author-new/author-new.component';
import { AuthorUpdateComponent } from './authors/author-update/author-update.component';

import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import { IndexComponent } from './index/index.component';
import {ConferenceService} from "./shared/conference.service";
import { ConferenceDetailComponent } from './index/conference-detail/conference-detail.component';
import { AboutComponent } from './about/about.component';
import { ConferenceNewComponent } from './index/conference-new/conference-new.component';
import { RegisterReviewerComponent } from './index/register-reviewer/register-reviewer.component';
import { RegisterAuthorComponent } from './index/register-author/register-author.component';
import { RegisterListenerComponent } from './index/register-listener/register-listener.component';
import { LoginComponent } from './login/login.component';
import { LoginReviewerComponent } from './login-reviewer-dobrea/login-reviewer.component';
import { ReviewerDashboardComponent } from './reviewer-dashboard-dobrea/reviewer-dashboard.component';
import { DashboardAuthorComponent } from './dashboard-author/dashboard-author.component';
import { DashboardReviewerBiddingComponent } from './dashboard-reviewer-bidding/dashboard-reviewer-bidding.component';
import { DashboardReviewerEvaluationComponent } from './dashboard-reviewer-evaluation/dashboard-reviewer-evaluation.component';
import { ListenerDashboard } from './listener.dashboard/listener.dashboard.component';




@NgModule({
  declarations: [
    AppComponent,
    
    AuthorsComponent,
    AuthorListComponent,
    AuthorDetailComponent,
    AuthorNewComponent,
    AuthorUpdateComponent,

    NavbarComponent,
    FooterComponent,
    IndexComponent,
    ConferenceDetailComponent,
    AboutComponent,
    ConferenceNewComponent,
    RegisterReviewerComponent,
    RegisterAuthorComponent,
    RegisterListenerComponent,
    LoginComponent,
    LoginReviewerComponent,
    ReviewerDashboardComponent,
    DashboardAuthorComponent,
    DashboardReviewerBiddingComponent,
    DashboardReviewerEvaluationComponent,
    ListenerDashboard,


  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    NgbModule.forRoot()
  ],
  providers: [ConferenceService, AuthorService],
  bootstrap: [AppComponent]
})
export class AppModule { }
