import {Component, OnInit} from '@angular/core';
import {Author} from "../../shared/author.model";
import {AuthorService} from "../../shared/author.service";
import {Router} from "@angular/router";
import {ReviewerService} from "../../shared/reviewer.service";
import {Reviewer} from "../../shared/reviewer.model";
import {DataSharingService} from "../../shared/data-sharing-service";

@Component({
    selector: 'app-author-list',
    templateUrl: './author-list.component.html',
    styleUrls: ['./author-list.component.css']
})
export class AuthorListComponent implements OnInit {
    errorMessage: string;
    authors: Array<Author>;
    reviewers: Array<Reviewer>;

    constructor(private authorService: AuthorService,
                private revService: ReviewerService,
                private dataSharing: DataSharingService,
                private router: Router) {
    }

    ngOnInit(): void {
        this.getAuthors();
        this.getReviewers();
    }

    getAuthors() {
        this.authorService.getAuthors(1)
            .subscribe(
                authors => this.authors = authors,
                error => this.errorMessage = <any>error
            );
    }

    getReviewers() {
        this.revService.getReviewers(this.dataSharing.currentConferenceId)
            .subscribe(reviewers => this.reviewers = reviewers);
    }

    delete(id: number) {
        this.authorService.delete(id).subscribe(_ => {
                console.debug("author deleted");
                this.ngOnInit();
            },
            err => console.error("error deleting author", err));
    }

    update(id: number): void {
        this.router.navigate(['/chair/update-author', id]);
    }

    validate(author: Author) {
        author.validated = true;
    }

}
