import { Component, OnInit } from '@angular/core';
import {AuthorService} from "../../shared/author.service";
import {Location} from "@angular/common";

@Component({
  selector: 'app-author-new',
  templateUrl: './author-new.component.html',
  styleUrls: ['./author-new.component.css']
})
export class AuthorNewComponent implements OnInit {

  constructor(private authorService: AuthorService,
    private location: Location) { }

  ngOnInit() {
  }

  save(firstName, lastName, email, username, password) {
    console.log("save button pressed", firstName, lastName, email, username, password);
    let validated: boolean = false;
    this.authorService.save(firstName, lastName, email, username, password, validated)
      .subscribe(_ => {
          console.debug("author saved");
          this.location.back();
        },
        err => console.error("error saving author", err));
  }

}
