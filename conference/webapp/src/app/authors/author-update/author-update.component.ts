import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Params, Router} from "@angular/router";
import {switchMap} from "rxjs/operators";
import {AuthorService} from "../../shared/author.service";
import {Author} from "../../shared/author.model";


@Component({
  selector: 'app-author-update',
  templateUrl: './author-update.component.html',
  styleUrls: ['./author-update.component.css']
})
export class AuthorUpdateComponent implements OnInit {
  @Input() author: Author;

  constructor(private authorService: AuthorService,
              private router: Router,
              private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.route.params.pipe(
      switchMap((params: Params) => this.authorService.getAuthor(+params['id'])))
      .subscribe(author => this.author = author);
  }

  goBack(): void {
    this.router.navigate(['/chair/list-personnel']);
  }

  save(): void {
    this.authorService.update(this.author).subscribe(_ => this.goBack());
  }
}
