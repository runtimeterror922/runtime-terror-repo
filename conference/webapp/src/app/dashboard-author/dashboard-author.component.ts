import {Component, OnInit, Input} from '@angular/core';
import {Author} from "../shared/author.model";
import {ProposalService} from "../shared/proposal.service";
import {AuthorService} from "../shared/author.service";
import {DataSharingService} from "../shared/data-sharing-service";


@Component({
    selector: 'app-dashboard-author',
    templateUrl: './dashboard-author.component.html',
    styleUrls: ['./dashboard-author.component.css']
})
export class DashboardAuthorComponent implements OnInit {
    @Input() file: File = null;
    submission: boolean = true;
    abstract: boolean = true;
    proposalService: ProposalService;
    sharing: DataSharingService;
    authorsService: AuthorService;
    authors: Array<Author>;

    isReferenceSucesfullySubmitted:boolean=false;

    constructor(proposalService: ProposalService, sharing: DataSharingService, authorsService: AuthorService) {
        this.proposalService = proposalService;
        this.sharing = sharing;
        this.authorsService = authorsService
    }

    ngOnInit() {
        this.authorsService.getAuthors(1)
            .subscribe(authors => this.authors=authors)
    }

    changeForm() {
        this.ngOnInit();
    }

    findByUsername(username) {
        let result=this.authors.filter(author => author.username === username);
        //console.log(result)
        if(result==null){
            console.log("Wrong user");
            return -1;
        }
        else
            return result[0].id
    }

    saveAbstract(title, keywords, topics, authors, presenter) {
        console.log(title, keywords, topics);
        if (title==='' || keywords==='' || topics==='' || authors==='' || presenter==='') {
            alert("please fill in the required fields!");
            return;
        }
        let authorsUsernames = authors.split(" ");
        console.log((authorsUsernames));
        let authorsIds = authorsUsernames.map(user => this.findByUsername(user).toString());
        let presentingAuthorId = this.findByUsername(presenter).toString();
        this.proposalService.save(title, keywords, topics, "", "", authorsIds, presentingAuthorId)
            .subscribe(result=>this.isReferenceSucesfullySubmitted=true);
    }

    newProposal(){
        location.reload();
    }

    saveUpdate() {
        // NOT YET IMPLEMENTED
    }
}
