import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardReviewerBiddingComponent } from './dashboard-reviewer-bidding.component';

describe('DashboardReviewerBiddingComponent', () => {
  let component: DashboardReviewerBiddingComponent;
  let fixture: ComponentFixture<DashboardReviewerBiddingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardReviewerBiddingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardReviewerBiddingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
