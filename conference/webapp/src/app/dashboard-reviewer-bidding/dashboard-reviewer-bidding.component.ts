import {Component, OnInit} from '@angular/core';
import {Proposal} from "../shared/proposal.model";
import {ProposalService} from "../shared/proposal.service";
import {DataSharingService} from "../shared/data-sharing-service";
import {ReviewerService} from "../shared/reviewer.service";
import {Router} from "@angular/router";

@Component({
    selector: 'app-dashboard-reviewer-bidding',
    templateUrl: './dashboard-reviewer-bidding.component.html',
    styleUrls: ['./dashboard-reviewer-bidding.component.css']
})
export class DashboardReviewerBiddingComponent implements OnInit {
    //proposals: Array<Proposal>;

    constructor(private dataSharing: DataSharingService,
                private router: Router,
                private proposalService: ProposalService) {
    }

    ngOnInit() {
        if (this.dataSharing.finishedBidding) {
            this.router.navigate(['dashboard/reviewer-evaluation']);
        }
        this.proposalService.getProposals()
            .subscribe(proposals => this.dataSharing.submittedProposals = proposals);
    }

    submit(proposal: Proposal) {
        if (proposal.radiovalue === "yes") {
            this.dataSharing.toBeEvaluatedProposals.push(proposal)
        } else if (proposal.radiovalue === "no") {
            this.dataSharing.notToBeEvaluatedProposals.push(proposal)
        } else {
            alert("no value selected!");
            return;
        }
        this.dataSharing.submittedProposals = this.dataSharing.submittedProposals.filter(prop => prop.id != proposal.id);
        if(this.dataSharing.submittedProposals.length==0){
            this.dataSharing.finishedBidding=true;
            this.router.navigate(['dashboard/reviewer-evaluation']);
        }
    }
}
