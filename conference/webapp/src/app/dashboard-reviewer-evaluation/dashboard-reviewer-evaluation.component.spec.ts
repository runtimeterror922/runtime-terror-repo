import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardReviewerEvaluationComponent } from './dashboard-reviewer-evaluation.component';

describe('DashboardReviewerEvaluationComponent', () => {
  let component: DashboardReviewerEvaluationComponent;
  let fixture: ComponentFixture<DashboardReviewerEvaluationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardReviewerEvaluationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardReviewerEvaluationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
