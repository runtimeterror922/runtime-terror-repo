import { Component, OnInit } from '@angular/core';
import {Proposal} from "../shared/proposal.model";
import {DataSharingService} from "../shared/data-sharing-service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-dashboard-reviewer-evaluation',
  templateUrl: './dashboard-reviewer-evaluation.component.html',
  styleUrls: ['./dashboard-reviewer-evaluation.component.css']
})
export class DashboardReviewerEvaluationComponent implements OnInit {

    proposals: Proposal[];

    constructor(private dataSharing: DataSharingService,
                private router: Router) {
    }

    ngOnInit() {
        /*let p1 = new Proposal(1,2,"Incoherency","best, stuff, ever, period","random, notopic","incoherency_abstract.pdf","incoherency_presentation.ppt",["no"],"no");
        let p2 = new Proposal(1,2,"Consistency","worst, stuff, ever, period","determined, onlyonetopic","consistency.pdf","consistency2.ppt",["no"],"no");
        let p3 = new Proposal(1,2,"Unity","zilliax","hearthstone","abstract.pdf","presentation.ppt",["no"],"no");
        let p4 = new Proposal(1,2,"Precision","mechs, tribe","tonic, hearthstone","abs.pdf","prz.ppt",["no"],"no");
        let p5 = new Proposal(1,2,"Perfection","no, keywords, needed","about, without","perfection.pdf","failment.ppt",["no"],"no");
        this.proposals=[p1,p2,p3,p4,p5]*/

        if(this.dataSharing.toBeEvaluatedProposals.length==0){
            this.router.navigate(['conferences/detail/1']);
        }
        this.dataSharing.toBeEvaluatedProposals.forEach(p => p.radiovalue=undefined);
    }

    submit(proposal:Proposal) {
        console.log(proposal.radiovalue);
        if(proposal.radiovalue>"0"){
            this.dataSharing.acceptedProposals.push(proposal)
        } else if (proposal.radiovalue<="0"){
            this.dataSharing.refusedProposals.push(proposal)
        } else if (proposal.radiovalue === "yes" || proposal.radiovalue==undefined) {
            alert("no value selected!");
            return;
        }
        this.dataSharing.toBeEvaluatedProposals=this.dataSharing.toBeEvaluatedProposals.filter(p=>p.id!=proposal.id);
        if(this.dataSharing.toBeEvaluatedProposals.length==0){
            this.router.navigate(['conferences/detail/1']);
        }
    }
}
