import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Params, Router} from "@angular/router";
import {switchMap} from "rxjs/operators";
import {Conference} from "../../shared/conference.model";
import {ConferenceService} from "../../shared/conference.service";
import {DataSharingService} from "../../shared/data-sharing-service";

@Component({
    selector: 'app-conference-detail',
    templateUrl: './conference-detail.component.html',
    styleUrls: ['./conference-detail.component.css']
})
export class ConferenceDetailComponent implements OnInit {
    conference: Conference;

    constructor(private conferenceService: ConferenceService,
                private dataSharing:DataSharingService,
                private route: ActivatedRoute,
                private router: Router) {
    }

    ngOnInit(): void {
        window.scroll(0,0);
        this.route.params.pipe(
            switchMap((params: Params) => this.conferenceService.getConference(+params['id'])))
            .subscribe(conference => {
                conference.startingTime = new Date(conference.startingTime);
                conference.endingTime = new Date(conference.endingTime);
                conference.abstractDeadline = new Date(conference.abstractDeadline);
                conference.fullpaperDeadline = new Date(conference.fullpaperDeadline);
                conference.biddingDeadline = new Date(conference.biddingDeadline);
                this.conference = conference
            });
    }

    regReviewer(id: number) {
        this.router.navigate(['/conferences/reg-reviewer' ,id]);
    }

    regAuthor(id: number) {
        this.router.navigate(['/conferences/reg-author', id]);
    }

    regListener(id: number) {
        this.router.navigate(['/conferences/reg-listener', id]);
    }

    login(id: number) {
        this.dataSharing.currentUserId=id;
        this.router.navigate(['/login']);
    }
}
