import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router'
import {ConferenceService} from "../../shared/conference.service";

@Component({
    selector: 'app-conference-new',
    templateUrl: './conference-new.component.html',
    styleUrls: ['./conference-new.component.css']
})
export class ConferenceNewComponent implements OnInit {

    constructor(private conferenceService: ConferenceService,
                private router: Router) {

    }

    ngOnInit() {
    }

    save(name: string, callForPapers: string, start: string, end: string, abstract: string, fullpaper: string) {
        if (name==='' || callForPapers==='' || start==='' || end==='' || abstract==='' || fullpaper==='') {
            alert("please fill in the required fields!");
            return;
        }
        console.log(start);
        console.log(end);
        console.log(abstract);
        this.conferenceService.save(name, callForPapers, new Date(start), new Date(end), new Date(abstract), new Date(fullpaper))
            .subscribe(_ => {
                    console.debug("conference saved");
                    this.router.navigate(['/index']);
                },
                err => console.error("error saving conference", err));
    }

    goBack() {
        this.router.navigate(['/index']);
    }
}
