import {Component, OnInit} from '@angular/core';
import {ConferenceService} from "../shared/conference.service";
import {Router} from "@angular/router";
import {Conference} from "../shared/conference.model";

@Component({
    selector: 'app-index',
    templateUrl: './index.component.html',
    styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {
    title = 'Symposium';
    conferences: Conference[];

    constructor(private conferenceService: ConferenceService,
                private router: Router) {
    }

    ngOnInit() {
        this.conferenceService.getConferences()
            .subscribe(conferences => this.conferences = conferences.map(function (conf) {
                conf.startingTime = new Date(conf.startingTime);
                conf.endingTime = new Date(conf.endingTime);
                conf.abstractDeadline = new Date(conf.abstractDeadline);
                conf.fullpaperDeadline = new Date(conf.fullpaperDeadline);
                conf.biddingDeadline = new Date(conf.biddingDeadline);
                return conf;
            }));
    }

    goDetail(id: number): void {
        this.router.navigate(['/conferences/detail', id]);
    }

    scroll(el: HTMLElement) {
        el.scrollIntoView();
    }

    newConference() {
        this.router.navigate(['/conferences/new']);
    }
}
