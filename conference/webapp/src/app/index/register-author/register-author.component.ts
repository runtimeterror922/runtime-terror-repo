import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {AuthorService} from "../../shared/author.service";
import {Conference} from "../../shared/conference.model";
import {ConferenceService} from "../../shared/conference.service";
import {switchMap} from "rxjs/operators";

@Component({
    selector: 'app-register-author',
    templateUrl: './register-author.component.html',
    styleUrls: ['./register-author.component.css']
})
export class RegisterAuthorComponent implements OnInit {
    conference: Conference;
    showRegistered: String;

    constructor(private authorService: AuthorService,
                private conferenceService: ConferenceService,
                private router: Router,
                private route: ActivatedRoute) {
    }

    ngOnInit() {
        this.route.params.pipe(
            switchMap((params: Params) => this.conferenceService.getConference(+params['id'])))
            .subscribe(conference => {
                conference.startingTime = new Date(conference.startingTime);
                conference.endingTime = new Date(conference.endingTime);
                conference.abstractDeadline = new Date(conference.abstractDeadline);
                conference.fullpaperDeadline = new Date(conference.fullpaperDeadline);
                conference.biddingDeadline = new Date(conference.biddingDeadline);
                this.conference = conference
            });
    }

    save(firstName, lastName, email, username, password) {
        if (firstName==='' || lastName==='' || email==='' || username==='' || password==='') {
            alert("please fill in the required fields!");
            return;
        }
        this.showRegistered = 'dssdff';
    }

    // save(firstName, lastName, email, username, password) {
    //     console.log("save button pressed", firstName, lastName, email, username, password);
    //     let validated: boolean = false;
    //     this.authorService.save(firstName, lastName, email, username, password, validated)
    //         .subscribe(_ => {
    //                 console.debug("author saved");
    //                //TODO this.router.navigate(['/conferences/']);
    //             },
    //             err => console.error("error saving author", err));
    // }
    goBack() {
        this.router.navigate(['/conferences/detail', this.conference.id]);
    }
}
