import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListenerDashboard } from './listener.dashboard.component';

describe('ListenerDashboard', () => {
  let component: ListenerDashboard;
  let fixture: ComponentFixture<ListenerDashboard>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListenerDashboard ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListenerDashboard);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
