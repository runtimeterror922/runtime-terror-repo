import {Component, OnInit} from '@angular/core';
import {DataSharingService} from "../shared/data-sharing-service";
import {Router} from "@angular/router";

@Component({
    selector: 'app-listener.dashboard',
    templateUrl: './listener.dashboard.component.html',
    styleUrls: ['./listener.dashboard.component.css']
})
export class ListenerDashboard implements OnInit {


    constructor(private dataSharing: DataSharingService,
                private router: Router) {
    }

    ngOnInit() {
    }

    validate(id) {
      this.dataSharing.proposalAttendance[id]+=1;
    }
}
