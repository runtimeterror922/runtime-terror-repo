import { Component, OnInit } from '@angular/core';
import {ConferenceService} from "../shared/conference.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Conference} from "../shared/conference.model";
import {AuthorService} from "../shared/author.service";
import {Author} from "../shared/author.model";

@Component({
  selector: 'app-login-reviewer',
  templateUrl: './login-reviewer.component.html',
  styleUrls: ['./login-reviewer.component.css']
})
export class LoginReviewerComponent implements OnInit {

    conference:Conference;
    loginType: String;
    loginUsername:string;
    loginPassword:string;

    firstName: string;
    lastName: string;
    email: string;
    username: string;
    password: string;

    authors: Array<Author>;

  constructor(private authorService: AuthorService,private conferenceService: ConferenceService,
              private route: ActivatedRoute,private router: Router) { }

  ngOnInit() {
      this.loginType= this.route.snapshot.paramMap.get("loginType");
      var id= parseInt( this.route.snapshot.paramMap.get("conferenceId"), 10);
      this.conferenceService.getConference(id).subscribe(conf =>{this.conference=conf} );
      //this.getAuthors();
  }

    // getAuthors() {
    //     this.authorService.getAuthors()
    //         .subscribe(
    //             authors => this.authors = authors
    //         );
    // }

    login(){

        this.authors.forEach(author=>{
            if(author.username.localeCompare(this.loginUsername) && author.password.localeCompare(this.loginPassword))
                this.router.navigate(['/reviewerDashboard', this.conference.id]);
        })
    }

    /*
    For some unknown reason, the register doesn't work;
    Also, we should add a conferenceId field to Author, beacause authors can differ between conferences

    Also, verify who you login. At the moment you seach username for reviewers in Authors
     */

    register(){
      this.authorService.save(this.firstName,this.lastName,this.email,this.username,this.password,false);
      //this.getAuthors();
    }
}
