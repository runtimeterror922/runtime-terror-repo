import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {DataSharingService} from "../shared/data-sharing-service";
import {Author} from "../shared/author.model";
import {Reviewer} from "../shared/reviewer.model";
import {ReviewerService} from "../shared/reviewer.service";
import {AuthorService} from "../shared/author.service";

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    loginType: string = "reviewer";
    authors: Array<Author>;
    reviewers: Array<Reviewer>;
    badLogin:boolean=false;

    constructor(private dataSharing: DataSharingService, private revService: ReviewerService,
                private autService: AuthorService, private router: Router) {
    }

    ngOnInit() {
        this.autService.getAuthors(this.dataSharing.currentConferenceId)
            .subscribe(authors => this.authors = authors);
        this.revService.getReviewers(this.dataSharing.currentConferenceId)
            .subscribe(reviewers => this.reviewers = reviewers)
    }

    login(username, passsword) {
        console.log(this.loginType)
        if(username==="admin"){
            this.router.navigate(['chair/list-personnel']);
        }

        if (this.loginType==="reviewer") {
            let reviewer=this.reviewers.filter(reviewer=>reviewer.username===username && reviewer.password===passsword)
            console.log(reviewer)
            if(reviewer.length>0){
                this.router.navigate(['dashboard/reviewer-bidding']);
                this.dataSharing.currentUserId=reviewer[0].id
            }
            else{
                this.badLogin=true;
            }
        }
        if (this.loginType==="author") {
            let author=this.authors.filter(author=>author.username===username && author.password===passsword)
            if(author!=null){
                this.router.navigate(['dashboard/author']);
                this.dataSharing.currentUserId=author[0].id
            }
            else{
                this.badLogin=true;
            }
        }

        if (this.loginType==="listener") {
            this.router.navigate(['dashboard/listener']);
        }

    }

}
