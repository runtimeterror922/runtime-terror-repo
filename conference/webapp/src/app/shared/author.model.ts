export class Author {
  id: number;
  firstName: string;
  lastName: string;
  email: string;
  username: string;
  password: string;
  validated: boolean;
}
