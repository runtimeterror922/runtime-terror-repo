import {Injectable} from '@angular/core';

import {HttpClient} from "@angular/common/http";

import {Observable} from "rxjs";
import {map} from 'rxjs/operators';
import {Author} from "./author.model";

@Injectable()
export class AuthorService {
    private authorsUrl = 'http://localhost:8080/api/authors';

    constructor(private httpClient: HttpClient) {
    }

    getAuthors(currentConferenceId): Observable<Author[]> {
        return this.httpClient.get<Array<Author>>(this.authorsUrl + "/" + currentConferenceId);
    }

    getAuthor(id: number): Observable<Author> {
        return this.getAuthors(1).pipe(
            map(authors => authors.find(author => author.id === id)));
    }

    update(author): Observable<Author> {
        const url = `${this.authorsUrl}/${author.id}`;
        return this.httpClient.put<Author>(url, author);
    }

    save(firstName: string, lastName: string, email: string, username: string, password: string, validated: boolean): Observable<Author> {
        let author = {firstName, lastName, email, username, password, validated};
        return this.httpClient.post<Author>(this.authorsUrl, author);
    }

    delete(id: number): Observable<Author> {
        const url = `${this.authorsUrl}/${id}`;
        return this.httpClient.delete<Author>(url);
    }
}
