export class Conference {
  id: number;
  name: string;
  callForPapers: string;
  startingTime: Date;
  endingTime: Date;
  abstractDeadline: Date;
  fullpaperDeadline: Date;
  biddingDeadline: Date;
    constructor(
        id: number,
        name: string,
        callForPapers: string,
        startingTime: Date,
        endingTime: Date,
        abstractDeadline: Date,
        fullpaperDeadline: Date,
        biddingDeadline: Date,
    ) {
    }
}
