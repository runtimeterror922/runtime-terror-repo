import {Injectable} from '@angular/core';

import {HttpClient} from "@angular/common/http";

import {Conference} from "./conference.model";

import {Observable} from "rxjs";
import {map} from 'rxjs/operators';


@Injectable()
export class ConferenceService {
  private conferencesUrl = 'http://localhost:8080/api/conferences';

  constructor(private httpClient: HttpClient) {
  }

  getConferences(): Observable<Conference[]> {
      return this.httpClient.get<Array<Conference>>(this.conferencesUrl);
  }

  getConference(id: number): Observable<Conference> {
    return this.getConferences().pipe(
      map(conferences => conferences.find(conference => conference.id === id)));
  }

  update(conference): Observable<Conference> {
    const url = `${this.conferencesUrl}/${conference.id}`;
    return this.httpClient
      .put<Conference>(url, conference);
  }

  save(name: string, callForPapers: string, startingTime: Date, endingTime: Date, abstractDeadline: Date, fullpaperDeadline: Date): Observable<Conference> {
    let conference = {name, callForPapers, startingTime, endingTime, abstractDeadline, fullpaperDeadline};
    return this.httpClient
      .post<Conference>(this.conferencesUrl, conference);
  }
}
