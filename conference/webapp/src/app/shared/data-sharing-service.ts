import {Injectable} from "@angular/core";
import {Author} from "./author.model";
import {AuthorService} from "./author.service";
import {Router} from "@angular/router";
import {Proposal} from "./proposal.model";

@Injectable({
    providedIn: 'root',
})
export class DataSharingService {
    public currentUserId:number;
    public currentConferenceId:number=1;

    submittedProposals: Array<Proposal>=[];
    toBeEvaluatedProposals: Array<Proposal>=[];
    notToBeEvaluatedProposals: Array<Proposal>=[];

    acceptedProposals: Array<Proposal>=[];
    refusedProposals: Array<Proposal>=[];

    finishedBidding:boolean =false;

    proposalAttendance={
        1:0,
        2:0,
        3:0,
        4:0,
        5:0,
        6:0,
        7:0
    };

    constructor(private router: Router) {
    }

}
