export class Proposal {
    id: number;
    authorId:number;
    proposalTitle: string;
    keywords: string;
    topics: string;
    abstractPaper: string;
    fullPaper: string;
    authors: [string];
    presentingAuthor: string;
    radiovalue: string;


    constructor(id: number,authorId:number, proposalTitle: string, keywords: string, topics: string, abstractPaper: string,
                fullPaper: string, authors: [string], presentingAuthor: string) {
        this.id = id;
        this.authorId=authorId;
        this.proposalTitle = proposalTitle;
        this.keywords = keywords;
        this.topics = topics;
        this.abstractPaper = abstractPaper;
        this.fullPaper = fullPaper;
        this.authors = authors;
        this.presentingAuthor = presentingAuthor;
    }
}
