import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Proposal} from './proposal.model';
import {map} from 'rxjs/operators';
import {DataSharingService} from './data-sharing-service';

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json',
        // 'Authorization': 'my-auth-token'
    })
};

@Injectable({
    providedIn: 'root',
})
export class ProposalService {
    private proposalsUrl = 'http://localhost:8080/api/proposals';

    constructor(private dataSharing: DataSharingService,
                private httpClient: HttpClient) {
    }

    getProposals(): Observable<Proposal[]> {
        return this.httpClient.get<Array<Proposal>>(this.proposalsUrl + '/' + this.dataSharing.currentConferenceId);
    }

    getProposal(id: number): Observable<Proposal> {
        return this.getProposals().pipe(
            map(proposals => proposals.find(proposal => proposal.id === id)));
    }

    update(proposal): Observable<Proposal> {
        const url = `${this.proposalsUrl}/${proposal.id}`;
        return this.httpClient.put<Proposal>(url, proposal);
    }

    save(proposalTitle: string, keywords: string, topics: string, abstractPaper: string,
         fullPaper: string, authorsIds: [string], presentingAuthorId: string): Observable<Proposal> {
        const proposal = {proposalTitle, keywords, topics, authorsIds, presentingAuthorId, abstractPaper, fullPaper};
        console.log(proposal);
        return this.httpClient.post<Proposal>(this.proposalsUrl, proposal, httpOptions);
    }

    delete(id: number): Observable<Proposal> {
        const url = `${this.proposalsUrl}/${id}`;
        return this.httpClient.delete<Proposal>(url);
    }
}
