export class Reviewer {
    id;
    firstName:string;
    lastName:string;
    type:string;
    isDeadlineResponsible:boolean;
    affiliation:string;
    email:string;
    username:string;
    password:string;
    website:string;
}
