import {Injectable} from '@angular/core';

import {HttpClient} from "@angular/common/http";

import {Observable} from "rxjs";
import {Reviewer} from "./reviewer.model";

@Injectable({
    providedIn: 'root',
})
export class ReviewerService {
    private reviewersUrl = 'http://localhost:8080/api/reviewers';

    constructor(private httpClient: HttpClient) {
    }

    getReviewers(currentConferenceId): Observable<Reviewer[]> {
        return this.httpClient.get<Array<Reviewer>>(this.reviewersUrl + "/" + currentConferenceId);
    }

    /*getReviewer(id: number): Observable<Reviewer> {
        return this.getReviewers(1).pipe(
            map(authors => authors.find(reviewer => reviewer.id === id)));
    }

    update(reviewer): Observable<Reviewer> {
        const url = `${this.reviewersUrl}/${reviewer.id}`;
        return this.httpClient.put<Reviewer>(url, author);
    }

    save(firstName: string, lastName: string, email: string, username: string, password: string, validated: boolean): Observable<Reviewer> {
        let reviewer = {firstName, lastName, email, username, password, validated};
        return this.httpClient.post<Reviewer>(this.reviewersUrl, reviewer);
    }

    delete(id: number): Observable<Reviewer> {
        const url = `${this.reviewersUrl}/${id}`;
        return this.httpClient.delete<Reviewer>(url);
    }*/
}
