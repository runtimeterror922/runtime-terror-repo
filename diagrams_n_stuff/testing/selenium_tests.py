import time

from selenium import webdriver
import unittest
import os
from selenium.common.exceptions import NoSuchElementException


class ConferenceTesting(unittest.TestCase):

    # executed before every testcase
    # @classmethod #so its called once - can be omitted so setup before every unittest
    def setUp(self):
        print('setting up unit test case')
        path_to_driver = 'D:\\MyDocuments\\qa_training_codespring\\3vas\\chromedriver.exe'
        os.environ['webdriver.chrome.driver'] = path_to_driver
        self.driver = webdriver.Chrome(path_to_driver)
        self.driver.implicitly_wait(300)
        self.driver.maximize_window()


    # executed after every testcase
    # @classmethod
    def tearDown(self):
        print('tearing down unit test case')
        print('---------------------------')
        self.driver.quit()



    # must start with test_
    def test_login_reviewer(self):
        self.driver.get('http://localhost:4200/login')
        try:
            time.sleep(2)
            web_element = self.driver.find_element_by_id('username')
            web_element.send_keys('andy')
            time.sleep(2)
            web_element = self.driver.find_element_by_id('password')
            web_element.send_keys('andy')
            time.sleep(2)
            self.driver.find_element_by_xpath('//button[text()="LOGIN"]').click()
            time.sleep(4)
            self.assertEqual(self.driver.current_url, 'http://localhost:4200/dashboard/reviewer-bidding','url mismatch')
            print('login test successful')
        except NoSuchElementException as e:
            self.fail('could not login:', e)


    def test_scroll_conferences(self):
        self.driver.get('http://localhost:4200/index')
        try:
            time.sleep(3)
            self.driver.find_element_by_xpath('//button[text()="See hosted conferences"]').click()
            time.sleep(2)
            web_elements = self.driver.find_elements_by_tag_name('button')
            self.assertGreater(len(web_elements), 5, 'should be more than 5 buttons')
            print('conference browsing test successful')
        except NoSuchElementException as e:
            self.fail('wrong button nr:', e)


    def test_about_image(self):
        self.driver.get('http://localhost:4200/about')
        try:
            time.sleep(2)
            web_elements = self.driver.find_element_by_tag_name('img')
            self.assertIsNotNone(web_elements, 'should be an image here')
            print('image loading test successful')
        except NoSuchElementException as e:
            self.fail('no image:', e)


    def test_bad_login(self):
        self.driver.get('http://localhost:4200/login')
        try:
            time.sleep(2)
            web_element = self.driver.find_element_by_id('username')
            web_element.send_keys('andy')
            time.sleep(2)
            web_element = self.driver.find_element_by_id('password')
            web_element.send_keys('badpass')
            time.sleep(2)
            self.driver.find_element_by_xpath('//button[text()="LOGIN"]').click()
            time.sleep(2)
            web_element = self.driver.find_elements_by_xpath("//*[contains(text(), 'Invalid username or password')]")
            self.assertIsNotNone(web_element, 'error should pop up')
            print('error did pop up successfully')
            self.assertNotEqual(self.driver.current_url, 'http://localhost:4200/dashboard/reviewer-bidding','should not redirect')
            print('no redirection successful')
        except NoSuchElementException as e:
            self.fail('should not have redirected:', e)


    def test_nologin_redirect(self):
        self.driver.get('http://localhost:4200/index')
        try:
            time.sleep(2)
            self.driver.find_element_by_id('navbarDropdown3').click()
            time.sleep(2)
            self.driver.find_element_by_link_text('Reviewer - evaluation').click()
            time.sleep(2)
            self.assertEqual(self.driver.current_url, 'http://localhost:4200/conferences/detail/1','should redirect')
            print('nologin redirection test successful')
        except NoSuchElementException as e:
            self.fail('should have redirected:', e)


    def test_bidding(self):
        self.driver.get('http://localhost:4200/login')
        try:
            time.sleep(2)
            web_element = self.driver.find_element_by_id('username')
            web_element.send_keys('andy')
            web_element = self.driver.find_element_by_id('password')
            web_element.send_keys('andy')
            self.driver.find_element_by_xpath('//button[text()="LOGIN"]').click()
            time.sleep(2)
            web_elements = self.driver.find_elements_by_xpath('//button[text()="Submit"]')
            self.assertEqual(len(web_elements), 5, '5 buttons by start')
            self.driver.find_element_by_xpath('//button[text()="Submit"]').click()
            web_elements = self.driver.find_elements_by_xpath('//button[text()="Submit"]')
            self.assertEqual(len(web_elements), 4, '4 buttons now')
        except NoSuchElementException as e:
            self.fail('not good buttons:', e)


    def test_login_linkworks(self):
        self.driver.get('http://localhost:4200/login')
        try:
            time.sleep(2)
            web_element = self.driver.find_element_by_id('username')
            web_element.send_keys('andy')
            web_element = self.driver.find_element_by_id('password')
            web_element.send_keys('andy')
            self.driver.find_element_by_xpath('//button[text()="LOGIN"]').click()
            time.sleep(2)
            self.driver.get('http://localhost:4200/index')
            time.sleep(2)
            self.driver.find_element_by_id('navbarDropdown3').click()
            time.sleep(2)
            self.driver.find_element_by_link_text('Reviewer - bidding').click()
            time.sleep(2)
            self.assertEqual(self.driver.current_url, 'http://localhost:4200/dashboard/reviewer-bidding', 'should redirect')
        except NoSuchElementException as e:
            self.fail('not redirect:', e)


if __name__ == '__main__':
    unittest.main()
