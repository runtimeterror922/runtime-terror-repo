0. setup to setup

    0. open google to get help if you are stuck


1. database setup:

    0. you already have microsoft sql server installed

    1. https://www.microsoft.com/en-us/download/details.aspx?id=57782, download this, unzip it somewhere

    2. from this-unzipped-folder/sqljdbc_7.2/enu/auth copy the sqljdbc_auth.dll conforming your system (32/64bit)
        and paste it into your java home, something like: c:/programfiles/java/jdk-11/bin

    3. open microsoft sql server management studio and run this script:
        CREATE DATABASE conference_ms
        GO
        USE conference_ms
        GO
        CREATE LOGIN conference_user WITH PASSWORD = 'password'
        GO
        IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'conference_user')
        BEGIN
            CREATE USER [conference_user] FOR LOGIN [conference_user]
            EXEC sp_addrolemember N'db_owner', N'conference_user'
        END
        GO
    
    4. do the steps from this link:
        https://dzone.com/articles/configuring-spring-boot-for-microsoft-sql-server
        from "I too learned the hard way" until the end of that section (sql server dependencies title)


2. backend server setup:

    0. git clone the repo

    1. in intellij, file > new > project from existing sources
    
    2. navigate {bitbucket-repo-folder}/conference, select build.gradle and ok
    
    3. check autoimport and auto-directory-creation, set your java to a v11 compiler
        (if jou are using java8 (jdk1.8), download v11)

    4. gradle should download all the dependencies

    5. download the lombok plugin for intellij from the intellij marketplace

    6. in intellij>settings type annotation processing in the search bar and check enable annotation processing

    6. download tomcat as a zip, unzip it somewhere

    7. in intellij create a new tomcat config, here set the pat to the tomcat folder
    VERY IMPORTANT: at deployment tab, select the exploded rar and delete everything from the application context field

    8. now you should be able to run the server running the configuration
        (logger errors may appear, but dont mind them)

    9. to check if all ok, go to localhost:8080/api/conferences to see a bunch of json data


3. frontend server setup:

    0. install angular7 and npm

    1. navigate to /webapp in cmd or terminal

    2. run "npm install" (without quotes) to pull dependencies

    3. run "ng serve" or "npm run start" to start the dev server

    4. go to localhost:4200/index

    5. you will see all changes in real life